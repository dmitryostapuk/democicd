import cicd_demo
import json
import os

os.environ['ENDPOINT_URL'] = f'http://{os.environ.get("LOCALSTACK_HOST", "localhost")}:4566'
print(os.environ['ENDPOINT_URL'])
os.environ['queue_name'] = 'test_queue'
SQS_CLIENT = cicd_demo.get_sqs_client()


class TestSQS:

    def test_sqs_send_message(self):
        print(f'QUEUE: {os.environ["queue_name"]}')
        print(f'CONNECTION: {os.environ["ENDPOINT_URL"]}')
        SQS_CLIENT.create_queue(QueueName=os.environ['queue_name'])
        result = cicd_demo.sqs_send_message({'test': 'test'}, None)
        assert result.get('ResponseMetadata', {}).get('HTTPStatusCode') == 200
        result = cicd_demo.sqs_get_messages({}, None)
        assert json.loads(result[0].get('Body')) == {'test': 'test'}

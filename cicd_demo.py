import boto3
import json
import os


def get_sqs_client():
    sqs = boto3.client('sqs', endpoint_url=os.environ.get('ENDPOINT_URL'))
    return sqs


def sqs_send_message(event, context):
    queue_name = os.environ.get('queue_name')
    sqs = get_sqs_client()
    data = sqs.get_queue_url(QueueName=queue_name)
    response = sqs.send_message(QueueUrl=data.get('QueueUrl'), MessageBody=json.dumps(event))
    return response


def sqs_get_messages(event, context):
    queue_name = os.environ.get('queue_name')
    sqs = get_sqs_client()
    data = sqs.get_queue_url(QueueName=queue_name)
    result = []
    while True:
        response = sqs.receive_message(QueueUrl=data.get('QueueUrl'),
                                       MaxNumberOfMessages=2,
                                       WaitTimeSeconds=5)
        messages = response.get('Messages', [])
        if messages:
            for message in messages:
                print(message)
                result.append(message)
                sqs.delete_message(QueueUrl=data.get('QueueUrl'),
                                   ReceiptHandle=message.get('ReceiptHandle'))
        else:
            break
    return result

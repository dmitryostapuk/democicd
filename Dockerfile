FROM lambci/lambda:build-python3.6

COPY requirements.txt /var/task/

WORKDIR /var/task

RUN pip install --upgrade pip
RUN pip install --no-cache-dir --no-warn-script-location --user -r /var/task/requirements.txt

ENV PATH=$PATH:/root/.local/bin:/var/lang/bin
ENV PYTHONPATH=$PYTHONPATH:/var/task/
